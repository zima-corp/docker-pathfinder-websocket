FROM 6run0/pathfinder-base:latest

ARG PATHFINDER_WEBSOCKET_REPO=https://github.com/goryn-clade/pathfinder_websocket.git

RUN \
  set -eux; \
  git clone  --single-branch --depth 1 ${PATHFINDER_WEBSOCKET_REPO} .; \
  composer install \
  --ansi \
  --no-cache \
  --no-dev \
  --no-interaction \
  --no-progress \
  --optimize-autoloader \
  ; \
  rm -rf .git

EXPOSE 5555 8020

CMD ["php", "cmd.php", "--tcpHost", "0.0.0.0"]
